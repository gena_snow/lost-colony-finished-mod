README.txt

This mod adds the following new objects:
- EntityStat/Antifungal, default value 0
- ConsumbleItem/Antifungal, adds 5% to Antifungal stat over 5 seconds
- Package/DataCanister, does nothing but exist
* Possible mod conflicts: The EntityStatComponentDefinition for CharacterStatComponent/Default_Astronaut and CharacterStatComponent/Default_Astronaut_Female are defined in EntityComponents_LCF.sbc. If another mod tries to define these objects, they will conflict.

This mod changes the existing Communications Tower grid:
- Replaces an interior wall with a programmable block
- Replaces a freight container with a small cargo container and moves the old contents of the freight container to a different, existing container
- Replaces a freight container with an LCD panel and moves the old contents of the freight container to a different, existing container
- Replaces a grated catwalk with a button panel
* Possible issue: If this mod is applied after the Communications Tower grid has been modified by the player(s), the player(s)'s changes will be lost!

The new programmable block contains a script that:
- Checks if the antenna named "Antenna" is powered on
- Checks if a Package/DataCanister exists in the new small cargo container
- Prints a message on the new LCD screen with instructions and results

The new button panel:
- Runs the script on the new programmable block with default arguments