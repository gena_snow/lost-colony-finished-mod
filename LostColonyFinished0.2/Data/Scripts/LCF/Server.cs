﻿using System;
using System.Collections.Generic;
using System.Text;
using VRage.Game.Components;
using Sandbox.ModAPI;
using VRageMath;
using VRage.Game.ModAPI;
using VRage.ModAPI;
using Sandbox.Common.ObjectBuilders.Definitions;
using VRage.Game;
using Sandbox.Game.Entities;



namespace Gena.LCF
{
    [MySessionComponentDescriptor(MyUpdateOrder.BeforeSimulation | MyUpdateOrder.AfterSimulation)]
    class Server : MySessionComponentBase
    {
        public static Server Instance; // the only way to access session comp from other classes and the only accepted static.

        private static PlayerDataStore mPlayerDataStore = new PlayerDataStore();
        private static List<IMyPlayer> mPlayers = new List<IMyPlayer>();

        private static bool modStarted = false;
        private static int tick = 0;
        private int pause = 300;

        private static bool won = false;

        public static readonly ushort DRMH = 1341;
        public static readonly ushort UAFMH = 1342;

        private IMyTextPanel mailbox_lcd = null;

        public override void LoadData()
        {
            // amongst the earliest execution points, but not everything is available at this point.

            Instance = this;

        }

        public override void BeforeStart()
        {
            // executed before the world starts updating

            /* 
             * Check to see if "Communications Tower" exists
             * If so, remove it and spawn in "Updated Communications Tower"
             */
            var old_tower = MyAPIGateway.Entities.GetEntity((x) => x is IMyCubeGrid && x.DisplayName == "Communications Tower") as IMyEntity;
            if (old_tower != null)
            {
                Vector3D old_tower_position = old_tower.PositionComp.GetPosition();

                // Remove the old tower
                old_tower.Delete();

                // Add the new tower
                List<IMyCubeGrid> empty_list = new List<IMyCubeGrid> { };
                String new_tower_name = "Updated Communications Tower";

                //<Position x="-33116.897623334007" y="33968.441244395806" z="40685.2424154987" />
                Vector3D position = new Vector3D(-33116.897623334007, 33968.441244395806, 40685.2424154987);

                //<Forward x="-0.4552677" y="0.465985656" z="-0.758675635" />
                Vector3D forward = new Vector3D(-0.4552677, 0.465985656, -0.758675635);

                //<Up x = "-0.5301875" y = "0.542669237" z = "0.6514686" />
                Vector3D up = new Vector3D(-0.5301875, 0.542669237, 0.6514686);

                Vector3 initialLinearVelocity = default(Vector3);
                Vector3 initialAngularVelocity = default(Vector3);
                string beaconName = null;
                // Align to World Matrix
                SpawningOptions spawningOptions = SpawningOptions.UseOnlyWorldMatrix;
                bool updateSync = false;
                Action callback = null;
                MyAPIGateway.PrefabManager.SpawnPrefab(empty_list, new_tower_name, old_tower_position, forward, up,
                    initialLinearVelocity, initialAngularVelocity, beaconName, spawningOptions, updateSync, callback);

                // Add items to chest
                var locker = MyAPIGateway.Entities.GetEntityById(85887793968125485).GetInventory() as IMyInventory;
                if (locker != null)
                {
                    var datapad = new MyObjectBuilder_Datapad
                    {
                        SubtypeName = "Datapad",
                        Name = "WE FOUND A CURE!!",
                        Data = "After months of work trapped without fresh supplies, we've finally found a way to cure the fungal infection. Those of us left are too weak to dig our way out, so all we can hope for is that someone will find this and share our data with the world. In this locker, you should find a few doses of the antifungal, as well as a data canister with the recipe. Send this data whoever you can reach!"
                    };
                    locker.AddItems(1, datapad);
                    var antifungal = new VRage.Game.MyObjectBuilder_ConsumableItem() { SubtypeName = "Antifungal" };
                    locker.AddItems(4, antifungal);
                    var datacanister = new MyObjectBuilder_Package() { SubtypeName = "DataCanister" };
                    locker.AddItems(1, datacanister);
                }

            }

            GetMailboxLCD();

        }

        private void GetMailboxLCD()
        {
            // Get grid
            IMyCubeGrid grid = null;
            grid = MyAPIGateway.Entities.GetEntity(x => x.DisplayName == "Updated Communications Tower") as IMyCubeGrid;
            if(grid == null)
                return;

            // Get TerminalService
            IMyGridTerminalSystem ts = null;
            ts = MyAPIGateway.TerminalActionsHelper.GetTerminalSystemForGrid(grid) as IMyGridTerminalSystem;

            // Get Mailbox LCD
            mailbox_lcd = ts.GetBlockWithName("Mailbox LCD") as IMyTextPanel;

        }

        protected override void UnloadData()
        {
            // executed when world is exited to unregister events and stuff

            Instance = null; // important for avoiding this object to remain allocated in memory

            MyAPIGateway.Multiplayer.UnregisterMessageHandler(DRMH, DataRequestMsgHandler);
            MyAPIGateway.Multiplayer.UnregisterMessageHandler(UAFMH, UpdateAFMsgHandler);
        }

        public override void UpdateAfterSimulation()
        {
            // Pause for a few seconds before starting up
            if (tick < pause)
            {
                tick++;
                return;
            }
            tick = 0;

            try
            {
                // If this isn't the server or host or isn't offline, quit
                // MyAPIGateway.Multiplayer.ServerId == MyAPIGateway.Session.Player.SteamUserId
                bool host = (MyAPIGateway.Multiplayer.ServerId == MyAPIGateway.Session.Player.SteamUserId);
                //bool host = (MyAPIGateway.Multiplayer.IsServerPlayer(MyAPIGateway.Session.Player.Client));
                              
                if (!MyAPIGateway.Multiplayer.IsServer || (MyAPIGateway.Session.OnlineMode != MyOnlineModeEnum.OFFLINE && !host))
                {
                    return;
                }

                if (!modStarted)
                {
                    modStarted = true;
                    Init_Main();
                    pause = 500;
                }


            }
            catch (Exception e)
            {
                MyAPIGateway.Utilities.ShowMessage("ERROR", "Error: " + e.Message + "\n" + e.StackTrace);

            }

            if (!won)
            {
                if (mailbox_lcd == null)
                    GetMailboxLCD();

                if (mailbox_lcd.GetText().StartsWith("Broadcasting message."))
                {
                    // You've won.
                    // Broadcast to Client.WMH
                    mPlayers.Clear();
                    MyAPIGateway.Players.GetPlayers(mPlayers);
                    foreach (IMyPlayer player in mPlayers)
                    {
                        MyAPIGateway.Multiplayer.SendMessageTo(
                            Client.WMH,
                            Encoding.Unicode.GetBytes("We won"),
                            player.SteamUserId
                            );
                    }

                    // Set local "won"
                    won = true;
                }
            }
        }

        private void Init_Main()
        {
            mPlayerDataStore.Load();
            CheckIfWon();

            // Register message handler for client-to-server messages
            MyAPIGateway.Multiplayer.RegisterMessageHandler(DRMH, DataRequestMsgHandler);
            MyAPIGateway.Multiplayer.RegisterMessageHandler(UAFMH, UpdateAFMsgHandler);

            //MyAPIGateway.Utilities.ShowMessage("Server", "Init finished");
        }

        public void CheckIfWon()
        {
            // Look through mPlayerDataStore at "won" for each player
            // If any are true, set local "won" to true
            foreach (ulong pdkey in mPlayerDataStore.mPlayerData.Keys)
            {
                if (mPlayerDataStore.mPlayerData[pdkey].won)
                {
                    won = true;
                    return;
                }
            }
        }

        private void UpdateAFMsgHandler(byte[] data)
        {
            // message tells us that client used the antifungal
            // update appropriately
            PlayerData mPlayerData = MyAPIGateway.Utilities.SerializeFromXML<PlayerData>(Encoding.Unicode.GetString(data));
            mPlayerDataStore.update(mPlayerData);
        }

        private void DataRequestMsgHandler(byte[] data)
        {
            //MyAPIGateway.Utilities.ShowMessage("Server", "Data request recieved");
            string message = Encoding.Unicode.GetString(data);
            ulong steamID = Convert.ToUInt64(message);

            //MyAPIGateway.Utilities.ShowMessage("Server", "Requestor is " + message);

            //Sends PlayerData from Server.cs to Client.cs to initialize character
            mPlayers.Clear();
            MyAPIGateway.Players.GetPlayers(mPlayers);
            IMyPlayer p = mPlayers.Find(x => x.SteamUserId.Equals(steamID));
            
            string out_message = MyAPIGateway.Utilities.SerializeToXML<PlayerData>(mPlayerDataStore.get(p));

            //MyAPIGateway.Utilities.ShowMessage("Server", "Preparing to send message: " + out_message);

            bool success = MyAPIGateway.Multiplayer.SendMessageTo(
                Client.AFDMH,
                Encoding.Unicode.GetBytes(out_message),
                steamID,
                true
            );
            if (success)
            {
                //MyAPIGateway.Utilities.ShowMessage("Server", "Data sent to client");
            }
        }

        public override void SaveData()
        {
            mPlayerDataStore.Save();
        }

    }
}
