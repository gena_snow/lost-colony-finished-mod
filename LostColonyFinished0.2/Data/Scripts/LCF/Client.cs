﻿using System;
using System.Text;
using VRage.Game.Components;
using VRage.Game;
using Sandbox.ModAPI;
using VRage.ModAPI;
using Sandbox.Game.Components;
using Sandbox.Game.Entities;
using VRage.Utils;

namespace Gena.LCF
{
    [MySessionComponentDescriptor(MyUpdateOrder.BeforeSimulation | MyUpdateOrder.AfterSimulation)]
    class Client : MySessionComponentBase
    {

        private PlayerData mPlayerData = null;
        private bool mStarted = false;

        private static int tick_count = 0;

        // channels for receiving messages
        public static readonly ushort AFDMH = 1340;
        public static readonly ushort WMH = 1343;

        //Make Init to register a messager handler (1340) to get messages from Server.cs
        public void Init()
        {
            // Register a message handler to recieve messages from the server
            MyAPIGateway.Multiplayer.RegisterMessageHandler(AFDMH, AntifungalDataMsgHandler);
            MyAPIGateway.Multiplayer.RegisterMessageHandler(WMH, WonMsgHandler);

        }

        public void WonMsgHandler(byte[] data)
        {

            // Display message on screen
            MyAPIGateway.Utilities.ShowMissionScreen("You've Saved Us All!", 
                "", null, "The message you've sent will allow medical " +
                "researchers to mass produce the antifungal. They can also " +
                "adapt it to an aerosol with which they can cleanse the " +
                "planet of the fungus. Good work!!", null, null);

            // Update mPlayerData
            mPlayerData.won = true;

            // Send updated mPlayerData to server
            string out_message = MyAPIGateway.Utilities.SerializeToXML<PlayerData>(mPlayerData);
            MyAPIGateway.Multiplayer.SendMessageToServer(
                Server.UAFMH,
                Encoding.Unicode.GetBytes(out_message)
                );

        }

        public void RequestData()
        {
            // Send a request for data to the server
            ulong steamID;
            steamID = this.Session.Player.SteamUserId;

            string message = steamID.ToString();
            bool success = MyAPIGateway.Multiplayer.SendMessageToServer(
                Server.DRMH,
                Encoding.Unicode.GetBytes(message)
            );

        }

        public override void UpdateAfterSimulation()
        {

            // If the session isn't available, quit
            if (MyAPIGateway.Session == null)
                return;


            // If not inited, call Init
            try
            {
                var isHost = MyAPIGateway.Session.OnlineMode == MyOnlineModeEnum.OFFLINE ||
                             MyAPIGateway.Multiplayer.IsServer;

                var isDedicatedHost = isHost && MyAPIGateway.Utilities.IsDedicated;

                if (isDedicatedHost)
                {
                    return;
                }

                if (!mStarted)
                {
                    mStarted = true;
                    Init();
                }

            }

            catch (Exception e)
            {
                MyAPIGateway.Utilities.ShowMessage("ERROR", "Error: " + e.Message + "\n" + e.StackTrace);
            }

            try
            {
                // Check if used_antifungal is false and antifungal > 0
                // If so, pop up message box and change used_antifungal to true
                /*
                if (!Sandbox.Game.MyVisualScriptLogicProvider.GameIsReady)
                {
                    MyAPIGateway.Utilities.ShowMessage("VisualScript", "Game is not ready");
                    return;
                }
                */
                if (mPlayerData == null)
                {
                    // Pause for a few seconds before starting up
                    tick_count++;
                    if (tick_count >= 500)
                    {
                        RequestData();
                        tick_count = 0;
                    }
                    return;
                }

                if (!mPlayerData.used_antifungal)
                {
                    IMyEntity entity = null;

                        entity = MyAPIGateway.Entities.GetEntityById(this.Session.Player.Character.EntityId);

                    MyCharacterStatComponent stats = null;

                        stats = entity.Components.Get<MyEntityStatComponent>() as MyCharacterStatComponent;


                    MyEntityStat af_stat = null;
                    var dict = stats.TryGetStat(MyStringHash.GetOrCompute("Antifungal"), out af_stat);
                    if (dict && af_stat != null && af_stat.Value > 0)
                    {
                        mPlayerData.used_antifungal = true;
                        mPlayerData.antifungal = af_stat.Value;

                        // Build message
                        string message = "You are now cured of the fungus and immune to further infection!";
                        if (!mPlayerData.won)
                        {
                            message += " All that's left is to broadcast the recipe to the rest of the world.";
                        }

                        MyAPIGateway.Utilities.ShowMessage("Congratulations!", "You are now cured of the fungus and immune to further infection!");
                        MyAPIGateway.Utilities.ShowMissionScreen("You're Cured!","", null, message, null, null);


                        // Send message on 1342 to server that it's been used
                        string out_message = MyAPIGateway.Utilities.SerializeToXML<PlayerData>(mPlayerData);
                        MyAPIGateway.Multiplayer.SendMessageToServer(
                            Server.UAFMH,
                            Encoding.Unicode.GetBytes(out_message)
                        );
                    }


                }

            }
            catch (Exception e)
            {
                MyAPIGateway.Utilities.ShowMessage("UpdateAfterSimulation", "Something went wrong with checking for antifungal usage");
            }
            
        }

        private void AntifungalDataMsgHandler(byte[] data)
        {
            try
            {
                mPlayerData = MyAPIGateway.Utilities.SerializeFromXML<PlayerData>(Encoding.Unicode.GetString(data));

                if (mPlayerData == null)
                {
                    MyAPIGateway.Utilities.ShowMessage("ADMH", "Didn't recieve player data");
                    return;
                }

                // Get the IMyEntity and MyCharacterStatComponent for "me"
                IMyEntity entity = null;
                entity = MyAPIGateway.Entities.GetEntityById(this.Session.Player.Character.EntityId);
                if (entity == null)
                {
                    MyAPIGateway.Utilities.ShowMessage("ADMH", "Entity retrieval failed");
                    return;
                }
                MyCharacterStatComponent stats = null;
                stats = entity.Components.Get<MyEntityStatComponent>() as MyCharacterStatComponent;
                if (stats == null)
                {
                    MyAPIGateway.Utilities.ShowMessage("ADMH", "Stats retrieval failed");
                    return;
                }

                // Try to get the Antifungal stat if it's already saved with the character
                MyEntityStat af_stat = null;

                var statsDict = stats.TryGetStat(MyStringHash.GetOrCompute("Antifungal"), out af_stat);

                if (af_stat != null)
                {
                    // Set the stat to the starting value we got from the server
                    af_stat.Value = mPlayerData.antifungal;
                }
                else
                {
                    // There's a problem
                    MyAPIGateway.Utilities.ShowMessage("Antifungal error", "Stat doesn't exist");
                    return;
                }

                //MyAPIGateway.Utilities.ShowMessage("Client", "mPlayerData recieved");

                // It would be nice to get some indication that you're (still) sick at
                // this point
                if (!mPlayerData.used_antifungal)
                {
                    MyAPIGateway.Utilities.ShowMessage("Health Warning", "Your suit indicates that an unrecognized foreign microbe has entered your bloodstream.");
                }
            } catch (Exception e)
            {
                MyAPIGateway.Utilities.ShowMessage("ADMH", "Something went wrong");
            }

        }

        protected override void UnloadData() // will act up without the try-catches. yes it's ugly and slow. it only gets called on disconnect so we don't care
        {
            try
            {
                MyAPIGateway.Multiplayer.UnregisterMessageHandler(AFDMH, AntifungalDataMsgHandler);
                MyAPIGateway.Multiplayer.UnregisterMessageHandler(WMH, WonMsgHandler);
            }
            catch (Exception e)
            {
                MyAPIGateway.Utilities.ShowMessage("Error", e.Message + "\n" + e.StackTrace);
            }


        }
    }
}
