﻿using System.Xml.Serialization;
using VRage.ModAPI;

namespace Gena.LCF
{
    public class PlayerData
    {
        public ulong steamid;
        public long playerId;
        public float antifungal;
        public bool used_antifungal;
        public bool won;

        [XmlIgnoreAttribute]
        public VRage.Game.MyCharacterMovementEnum lastmovement;

        [XmlIgnoreAttribute]
        public IMyEntity entity;

        [XmlIgnoreAttribute]
        public bool loaded;

        public PlayerData(ulong id)
        {
            steamid = id;
            playerId = 0;
            antifungal = 0;
            used_antifungal = false;
            won = false;
            lastmovement = 0;
            entity = null;
            loaded = false;
        }

        public PlayerData()
        {
            playerId = 0;
            antifungal = 0;
            used_antifungal = false;
            won = false;
            lastmovement = 0;
            entity = null;
            loaded = false;
        }
    }
}
